module.exports = {
  important: false,
  purge: ['./src/**/*.html', './src/**/*.vue'],
  theme: {
    screens: {
      xl: { max: '1281px' },
      lg: { max: '1025px' },
      md: { max: '769px' },
      sm: { max: '641px' }
    },

    extend: {

      fontSize: {
        32: '32px',
        28: '28px',
        24: '24px',
        20: '20px',
        14: '14px',
        12: '12px'
      },

      spacing: {
        input: '2.5rem',
        navbar: '3.75rem'
      },

      maxWidth: {
        '7xl': '80rem'
      },

      colors: {
        disabled: '#DEE2E6',
        green: '#46B800'
      }

    }
  },
  variants: {},
  plugins: []
}
