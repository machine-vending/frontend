export const state = () => ({
  user: null,
  auth: null
})

export const mutations = {
  setUser(state, payload) {
    state.user = payload
  },
  setAuth(state, payload) {
    state.auth = payload
  },
  clearAuth(state) {
    state.auth = null
    state.user = null
  }
}
