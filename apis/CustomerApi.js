import axios from "./instance"

export default {
  getProductsByMachine: id => {
    return axios.api.get(`/api/v1/machine/${id}`).then(response => response.data)
  },
  createOrder: params => {
    return axios.api.post(`/api/v1/order`, params).then(response => response.data)
  },
  getMachines: () => {
    return axios.api.get(`/api/v1/machine`).then(response => response.data)
  },
}
