import axios from "axios"
import apiConfig from "~/configs/endpoint"

let apiInstance = {
  api: null,
  context: null,
}

export const createInstance = context => {
  const { store, redirect, route } = context
  const instance = axios.create({
    baseURL: apiConfig.ENV
  })
  instance.interceptors.request.use(config => {
    // if (store.state.main.auth && store.state.main.auth.accessToken) {
    //   config.headers["Authorization"] =
    //     store.state.main.auth.tokenType +
    //     " " +
    //     store.state.main.auth.accessToken
    // }
    // config.headers.Website = apiConfig.WEBSITE
    return config
  })

  instance.interceptors.response.use(
    response => {
      return response
    },
    error => {
      throw error
    }
  )

  apiInstance.api = instance
}

export default apiInstance
