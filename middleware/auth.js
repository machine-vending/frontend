export default function({ isHMR, app, store, route, params, error, redirect }) {
  const routerUnneedAuth = [
    "index",
    "login",
    // "login-token",
  ]
  if (!routerUnneedAuth.includes(route.name)) {
    if (!store.state.main.auth) {
      redirect("/")
    }
  }
}
