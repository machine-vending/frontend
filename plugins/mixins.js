import Vue from "vue"
import { mapState } from "vuex"

// Global Components
// Vue.component("FormInput", FormInput)
// Vue.component("Input", Input)
// Vue.component("Flickity", Flickity)

// Vue global lib
// Vue.use(Vuelidate)
// Vue.use(VueClipboard)
Vue.config.ignoredElements = [/^ion-/]

Vue.mixin({
  computed: {
    ...mapState({
      user: state => state.main.user,
      auth: state => state.main.auth
    }),
  },
  methods: {
    isNumber(num) {
      return new Intl.NumberFormat("th-TH", {
        maximumFractionDigits: 2,
        minimumFractionDigits: 2,
      })
        .format(num)
        .split(".")[0]
    },
  }
})
