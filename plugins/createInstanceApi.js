import { createInstance } from "~/apis/instance"

export default context => {
  createInstance(context)
}
