export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  mode: "spa",
  head: {
    title: "VENDING-MACHINE",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    { src: "~/assets/FONT/Sukhumvit/styles.css" },
    { src: "~/styles/default.scss" },
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "~/plugins/mixins.js" },
    { src: "~/plugins/createInstanceApi.js", ssr: false },
    { src: "~/plugins/vuexPersist.js", ssr: false },
  ],

  styleResources: {
    scss: [
      "./styles/extends/*.scss",
      "./styles/extends/_var.scss",
      "./styles/extends/_mixins.scss",
      "./styles/extends/_functions.scss",
    ],
  },

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: false,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    "@nuxtjs/dotenv",
    "@nuxtjs/style-resources",
    "@nuxtjs/tailwindcss",
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [],
  server: {
    port: process.env.PORT || 3001, // default: 3000
    host: "0.0.0.0", // default: localhost
  },
  router: {
    middleware: ["auth"],
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    babel: {
      presets({ isServer }) {
        const targets = isServer ? { node: "current" } : { ie: 11 }
        return [[require.resolve("@babel/preset-env"), { targets }]]
      },
      plugins: [
        "@babel/syntax-dynamic-import",
        "@babel/transform-runtime",
        "@babel/transform-async-to-generator",
      ],
    },
  },
}
